const server = require('express')();
const { createBundleRenderer } = require('vue-server-renderer');
const createApp = require('/path/to/built-server-bundle.js');

const renderer = createBundleRenderer(serverBundle, {
  runInNewContext: false, // рекомендуется
  template, // (опционально) шаблон страницы
  clientManifest // (опционально) манифест клиентской сборки
});

server.get('*', (req, res) => {
  const context = { url: req.url }

  createApp(context).then(app => {
    renderer.renderToString(app, (err, html) => {
      if (err) {
        if (err.code === 404) {
          res.status(404).end('Страница не найдена')
        } else {
          res.status(500).end('Внутренняя ошибка сервера')
        }
      } else {
        res.end(html)
      }
    })
  })
})

server.listen(8080)