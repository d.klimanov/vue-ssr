import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export function createStore () {
  return new Vuex.Store({
    state: () => ({
      user: {},
    }),

    actions: {
      fetchUser({ commit }) {
        return axios.get('http://localhost:3001/users/me').then((data) => {
          commit('setItem', data.data);
        });
      }
    },
    
    getters: {
      getUser(state) {
        return state.user;
      },
    },

    mutations: {
      setUser(state, value) {
        state.user = value;
      },
    },
  });
}
