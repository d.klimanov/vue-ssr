import Vue from 'vue';
import App from './App.vue';
import { createRouter } from './router';
import { createStore } from './store';
import { sync } from 'vuex-router-sync';

export function createApp () {
  // Создаём экземпляры маршрутизатора и хранилища
  const router = createRouter();
  const store = createStore();
  // Синхронизируем чтобы состояние маршрута было доступно как часть хранилища
  sync(store, router);

  // Создадим экземпляр приложения, внедряя и маршрутизатор и хранилище
  const app = new Vue({
    router,
    store,
    render: h => h(App),
  });

  // Возвращаем приложение, маршрутизатор и хранилище.
  return { app, router, store }
}
